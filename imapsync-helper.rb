#!/usr/bin/env ruby

# Interactive mode to run account by account
# Flags to override config

require "yaml"
require "awesome_print"
require "progressbar"
require "trollop"

# Specific to SR
def backup(account)
  account["account"].sub(/2323/, "0000")
end

def configure
  # Configuration
  $config = YAML.load_file("config/imapsync.yml")
  $config["accounts"].each do |account|
    account["backup"] = backup(account) if account["backup"].to_s.empty?
  end
  # ap $config
end

# Empty or <all> for batch, or only <given account> that appears in the configuraion
def arg_ok?(arg)
  $opts[arg].to_s.empty? or $opts[arg].downcase == "all" or $config["accounts"].map {|acct| acct["account"]}.include?($opts[arg])
end

def options_ok?
  %i(backup restore).all? {|arg| arg_ok?(arg)}
end

def only_one_present?
  %i(backup restore interactive).one? {|arg| $opts[arg] and not $opts[arg].nil?}
end

def command_line
  # Hash the command line - overrides configuration
  $opts = Trollop::options do
    version "ImapSync-helper v0.9"
    banner <<-EOS
imapsync-helper

Usage:
       imapsync-helper [options]

Where the options are:
EOS
    opt :interactive, "Interactive mode - ask all accounts"
    opt :backup, "Backup <all> or <given account>", :type => :string
    opt :restore, "Restore <all> or <given account>", :type => :string
  end
  Trollop::die "Problem with arguments" unless options_ok?
  Trollop::die "Please use exactly one option" unless only_one_present?
end

def base_command
  $config["flags"].inject("./imapsync.sh") { |s,f| s << " --#{f}"}
end

def command!(account, backup)
  if backup
    user1 = account["account"]
    user2 = account["backup"]
  else
    user1 = account["backup"]
    user2 = account["account"]
  end
  " --password1 #{account["pwd"]} --host1 #{$config["providers"][account["provider"]]}" <<
  " --password2 #{account["pwd"]} --host2 #{$config["providers"][account["provider"]]}" <<
  " --user1 #{user1} --user2 #{user2}"
end

def run_imapsync(account, backup)
  begin
    exec_string = base_command << command!(account, backup)
    ap exec_string
    `#{exec_string}`
    $?.exitstatus # $CHILD_STATUS
  rescue Exception => e
    ap e
  end
end

# http://stackoverflow.com/questions/174933/how-to-get-a-single-character-without-pressing-enter
def get_char
  state = `stty -g`
  `stty raw -echo -icanon isig`
  STDIN.getc.chr
ensure
  `stty #{state}`
end

def interactive
  $config["accounts"].each do |account|
    puts "Account #{account["account"]} and backup #{account["backup"]} - Default=[S]kip, [B]ackup or [R]estore?"
    # inp = gets.chomp.to_s.downcase
    case get_char
    when /[s\r\n]/
      next
    when "b"
      run_imapsync(account, true)
    when "r"
      run_imapsync(account, false)
    else
      redo
    end
  end
end

def batch(backup)
  pbar = ProgressBar.new "Accounts", $config["accounts"].count
  $config["accounts"].each do |account|
    pbar.inc
    run_imapsync(account, backup)
  end
  pbar.finish
end

# fix command line help + arguments
def main
  begin
    configure
    command_line
    if $opts[:interactive]
      interactive
    else
      backup = $opts[:backup]
      opt_account = $opts[:backup] || $opts[:restore]
      if (opt_account == "all")
        batch backup
      else
        run_imapsync($config["accounts"].select {|account| account["account"] == opt_account}.first, backup)
      end
    end
    puts ">> DONE <<"
  rescue Exception => e
    puts e
  end
end

# As executable - main
if __FILE__ == $PROGRAM_NAME
  main
end
